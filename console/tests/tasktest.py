import unittest
import toad.app as app
import instances.tasktypes as tt
import datetime as dt
import random
import os
import functions.schedulingfunctions as sfuncs
import functions.taskfunctions as tfunc
import configparser
from pathlib import Path
import shutil


class Tester(unittest.TestCase):
    def test_creating(self):
        task = tt.Task("asd", "qwe", dt.datetime.today(), "new task", 1, 0)
        self.assertNotEqual(task.task_id, None)
        self.assertEqual(task.name, "asd")

    def test_date(self):
        task = tt.Task("asd", "qwe", dt.datetime.today(), "new task", 1, 0)
        self.assertEqual(task.last_date.day, dt.datetime.today().day)

    def test_repr(self):
        task = tt.Task("asd", "qwe", dt.datetime.today(), "new task", 1, 0)
        self.assertEqual(task.__repr__() == tt.Task.task_count, True)

    def test_repr_sort(self):
        l = []
        for i in range(10):
            l.append(tt.Task(str(i), "qwe", dt.datetime.today(), "new task", 1, 0))

        random.shuffle(l)
        if not is_sorted(l):
            l = sorted(l, key=lambda task: tt.representations_for_sort["creation time"](task))
        self.assertEqual(is_sorted(l), True)

    def test_app_serialize(self):
        cfg = configparser.ConfigParser()
        cfg.read("configuration.ini")
        appl = app.Application(cfg, "DEBUG")
        appl.create_user()
        appl.create_manager()
        appl.add_task(tt.Task("asd", "desc", dt.datetime.today(), "", 0, 0))
        app.pickle_app(appl, cfg, "DEBUG")

    def test_app_deserialize(self):
        cfg = configparser.ConfigParser()
        cfg.read("configuration.ini")
        appl = app.Application(cfg, "DEBUG")
        appl.create_user()
        appl.create_manager()
        appl.add_task(tt.Task("asd", "desc", dt.datetime.today(), "", 0, 0))
        app.pickle_app(appl, cfg, "DEBUG")
        appl = app.unpickle_app(cfg, "DEBUG")
        self.assertEqual(appl.find_task(1).name, "asd")

    def test_app_find_task1(self):
        cfg = configparser.ConfigParser()
        cfg.read("configuration.ini")
        appl = app.Application(cfg, "DEBUG")
        appl.create_user()
        appl.create_manager()
        appl.add_task(tt.Task("asd", "desc", dt.datetime.today(), "", 0, 0))
        self.assertEqual(appl.find_task(tt.Task.task_count).name, "asd")

    def test_app_findtask2(self):
        cfg = configparser.ConfigParser()
        cfg.read("configuration.ini")
        appl = app.Application(cfg, "DEBUG")
        appl.create_user()
        appl.create_manager()
        head_t = tt.Task("sub", "desc", dt.datetime.today(), "", 0, 0)
        pid = head_t.task_id
        child_t = tt.Task("el", "desc", dt.datetime.today(), "", 0, 0)

        appl.add_task(head_t)
        appl.add_sub_task(child_t, pid)

        task1 = sfuncs.find_task_in_manager(appl.selected_manager, head_t.task_id)
        task2 = sfuncs.find_task_in_manager(appl.selected_manager, child_t.task_id)
        self.assertEqual(task1.name, "sub")
        self.assertEqual(task2.name, "el")

    def test_app_findtask3(self):
        cfg = configparser.ConfigParser()
        cfg.read("configuration.ini")
        appl = app.Application(cfg, "DEBUG")
        appl.create_user()
        appl.create_manager()
        big = tt.Task("big", "desc", dt.datetime.today(), "", 0, 0)
        sub = tt.Task("sub", "desc", dt.datetime.today(), "", 0, 0)
        el = tt.Task("el", "desc", dt.datetime.today(), "", 0, 0)
        appl.add_task(big)
        appl.add_sub_task(sub, big.task_id)
        appl.add_sub_task(el, sub.task_id)
        task1 = sfuncs.find_task_in_manager(appl.selected_manager, big.task_id)
        task2 = sfuncs.find_task_in_manager(appl.selected_manager, sub.task_id)
        task3 = sfuncs.find_task_in_manager(appl.selected_manager, el.task_id)
        self.assertEqual(task1.name, "big")
        self.assertEqual(task2.name, "sub")
        self.assertEqual(task3.name, "el")

    def test_plan_dayspan(self):
        cfg = configparser.ConfigParser()
        cfg.read("configuration.ini")
        appl = app.Application(cfg, "DEBUG")
        appl.create_user()
        appl.create_manager()
        plan_task = tt.Task("sub", "desc", dt.datetime.today(), "", 0, 0)
        id1 = plan_task.task_id
        id2 = id1 + 1
        appl.plan_task(plan_task, years=1, months=3)
        appl.check_overdued_tasks()
        appl.replan_tasks()
        task1 = sfuncs.find_task_in_manager(appl.selected_manager, id1)
        task2 = sfuncs.find_task_in_manager(appl.selected_manager, id2)
        self.assertEqual(task1.is_planned, False)
        self.assertEqual(task2.is_planned, True)

    def test_task_change(self):
        cfg = configparser.ConfigParser()
        cfg.read("configuration.ini")
        appl = app.Application(cfg, "DEBUG")
        appl.create_user()
        appl.create_manager()
        added_task = tt.Task("big", "desc", dt.datetime.today(), "", 0, 0)
        task_id = added_task.task_id
        appl.add_task(added_task)
        appl.change_task(task_id, description="ololo noone would read this")
        task1 = sfuncs.find_task_in_manager(appl.selected_manager, task_id)
        self.assertEqual(task1.description, "ololo noone would read this")

    def test_set_state(self):
        task1 = tt.Task("big", "desc", dt.datetime.today(), "", 0, 0)
        task2 = tt.Task("sub", "desc", dt.datetime.today(), "", 0, 0)
        task3 = tt.Task("big", "desc", dt.datetime.today(), "", 0, 0)
        task2.sub_tasks.append(task3)
        task1.sub_tasks.append(task2)
        tfunc.set_state(task1, tt.State.FAILED)
        self.assertEqual(task1.state, tt.State.FAILED)
        self.assertEqual(task2.state, tt.State.FAILED)
        self.assertEqual(task3.state, tt.State.FAILED)

    def test_shift_enumeration(self):
        task1 = tt.Task("big", "desc", dt.datetime.today(), "", 0, 0)
        id1 = task1.task_id
        task2 = tt.Task("sub", "desc", dt.datetime.today(), "", 0, 0)
        id2 = task2.task_id
        task3 = tt.Task("big", "desc", dt.datetime.today(), "", 0, 0)
        id3 = task3.task_id
        task2.sub_tasks.append(task3)
        task1.sub_tasks.append(task2)
        tfunc.shift_task_enumeration(task1)
        self.assertLess(id1, task1.task_id)
        self.assertLess(id2, task2.task_id)
        self.assertLess(id3, task3.task_id)

    def test_cancel_plan(self):
        cfg = configparser.ConfigParser()
        cfg.read("configuration.ini")
        appl = app.Application(cfg, "DEBUG")
        appl.create_user()
        appl.create_manager()
        plan_task = tt.Task("big", "desc", dt.datetime(year=2018, month=4, day=18), "", 0, 0)
        id1 = plan_task.task_id
        appl.plan_task(plan_task, years=1, months=3)
        self.assertEqual(plan_task.is_planned, True)
        appl.cancel_plan_task(id1)
        self.assertEqual(plan_task.is_planned, False)

    def test_find_nested(self):
        task1 = tt.Task("big", "desc", dt.datetime.today(), "", 0, 0)
        id1 = task1.task_id
        task2 = tt.Task("sub", "desc", dt.datetime.today(), "", 0, 0)
        id2 = task2.task_id
        task3 = tt.Task("big", "desc", dt.datetime.today(), "", 0, 0)
        id3 = task3.task_id
        task2.sub_tasks.append(task3)
        task1.sub_tasks.append(task2)
        found = tfunc.find_nested(task1, id2)
        self.assertEqual(found.name, "sub")

    def test_find_max_id(self):
        task1 = tt.Task("big", "desc", dt.datetime.today(), "", 0, 0)
        id1 = task1.task_id
        task2 = tt.Task("sub", "desc", dt.datetime.today(), "", 0, 0)
        id2 = task2.task_id
        task3 = tt.Task("big", "desc", dt.datetime.today(), "", 0, 0)
        id3 = task3.task_id
        task2.sub_tasks.append(task3)
        task1.sub_tasks.append(task2)
        max_id = tfunc.find_max_id(task1)
        self.assertEqual(task3.task_id, tt.Task.task_count)

    def setUp(self):
        try:
            os.mkdir(os.path.join(Path.home(), "binaries"))
        except FileExistsError:
            pass
        try:
            os.mkdir(os.path.join(Path.home(), "binaries/test"))
        except FileExistsError:
            pass

    def tearDown(self):
        shutil.rmtree(os.path.join(Path.home(), "binaries/test"))


def is_sorted(lst: list):
    is_sortd = True
    for i in range(len(lst)):
        if i > 0 and not (lst[i - 1] < lst[i]):
            is_sortd = False
            break
    return is_sortd


def main():
    unittest.main()


if __name__ == '__main__':
    main()

