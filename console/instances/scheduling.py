"""
module contains definition for class Manager
"""


class Manager:
    """
    task manager. contains task_dict with tasks and user_list
    with users, invited in this manager. first user in list is owner
    """
    manager_count = 0

    def __init__(self, users: list):
        Manager.manager_count += 1
        self.manager_id = Manager.manager_count
        self.task_dict = {}
        self.user_list = users

    def __str__(self):
        return str(self.manager_id)

    def __eq__(self, other):
        return self.manager_id == other
