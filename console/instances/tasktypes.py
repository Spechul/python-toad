"""
module contains defenition for task types and enumerations that are used in here
"""

import datetime as dt
import dateutil.relativedelta as rd
import instances.user as user
import copy


class State:
    IN_PROGRESS = "Progress"
    COMPLETED = "Completed"
    OUT_OF_TIME = "Out of time"
    FAILED = "failed"


class Task:
    """
        elementary task. has it's name, text, toad's comment, completion state, creator user
        representation form, priority, state "is_planned" and weekdays if is planned
    """
    task_count = 0

    def __init__(self, name: str, description: str, last_date: dt.datetime,
                 toad_comment: str, priority: int, user_id: int):
        self.name = name
        self.description = description
        self.toad_comment = toad_comment
        self.last_date = last_date
        self.creation_date = dt.datetime.today()
        self.priority = priority
        self.user_id = user_id
        self.state = State.IN_PROGRESS
        Task.task_count += 1
        self.task_id = Task.task_count
        self.is_planned = False
        self.replan_period = None
        self.planned_from = None
        self.manager_id = None
        self.archive = False
        self.replan_weekdays = []
        self.sub_tasks = []

    def __repr__(self):
        return self.task_id

    def __str__(self):
        return "name: " + self.name + " | description: " + self.description + " | id: " + \
               str(self.task_id) + " | planned: " + str(self.is_planned) + " | last date: " + str(self.last_date) + \
               " | priority " + str(self.priority) + " | state " + self.state

    def __lt__(self, other):
        return self.task_id < other.task_id


def name_representation(task: Task):
    return task.name


def priority_representation(task: Task):
    return task.priority


def creation_time_representation(task):
    return task.creation_date.time()


def user_id_representation(task: Task):
    return task.user_id


def task_id_representation(task: Task):
    return task.task_id


representations_for_sort = {
    "name": name_representation,
    "priority": priority_representation,
    "creation time": creation_time_representation,
    "user id": user_id_representation,
    "task id": task_id_representation,
    }

