from datetime import datetime
import sys


class ContextLogger:
    """
    do_log is 'yes' or other string. if other - logging messages won't be added
    messages is list of strings
    exception in "with" block will be written to log
    """
    def __init__(self, file_path: str, do_log: str):
        self.messages = []
        self.file_path = file_path
        self.do_log = do_log
        self.file = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            dir(exc_tb)
            self.error(str(exc_type.__name__) + ": " + str(exc_val))
        with open(self.file_path, 'a') as f:
                for line in self.messages:
                    f.write(line)
        if exc_type:
            return False

    def info(self, msg):
        if self.do_log == "yes":
            self.messages.append(str(datetime.today()) + " INFO: " + msg + "\n")

    def error(self, msg):
        if self.do_log == "yes":
            self.messages.append(str(datetime.today()) + " ERROR: " + msg + "\n")
