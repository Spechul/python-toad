"""
module contains class Application as a wrapper to functions package with it's own methods
"""
import instances.scheduling as scheduling
import functions.schedulingfunctions as sfuncs
import functions.taskfunctions as tfuncs
import instances.user as user
import instances.tasktypes as tt
import json
import datetime as dt
import pickle
import os
import configparser
import pathlib
from instances.inputerror import InputError
from toad.context_logger import ContextLogger
from dateutil import parser



class Application:
    """
    Application is the wrapper class that wraps(surprise!) all functions from the library.
    You can call them from it
    """
    def __init__(self, cfg: configparser.ConfigParser, run_type: str):

        self.logging_file = os.path.join(str(pathlib.Path.home()), cfg[run_type]["logging_file"])
        self.do_log = cfg[run_type]["logging"]

        self.users = []
        self.managers = []
        self.selected_user = None
        self.selected_manager = None

    def count_tasks(self):
        sfuncs.count_all_tasks(self.managers)

    def count_users(self):
        max_id = float("-inf")
        for usr in self.users:
            if usr.user_id > max_id:
                max_id = usr.user_id
        if max_id != float("-inf"):
            user.User.user_count = max_id
            return max_id
        else:
            return 0

    def count_managers(self):
        max = float("-inf")
        for man in self.managers:
            if man.manager_id > max:
                max = man.manager_id
        if max > float("-inf"):
            scheduling.Manager.manager_count = max
            return max
        else:
            return 0

    def create_manager(self):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            if self.selected_user is None:
                raise InputError("no user selected. first select or add user")
            man = scheduling.Manager([self.selected_user])

            logger.info("creating new manager")
            self.managers.append(man)
            self.selected_manager = man
            logger.info("new manager created " + str(man.manager_id))

    def add_manager(self, manager: scheduling.Manager):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("adding new manager")
            self.managers.append(manager)
            logger.info("added new manager " + str(manager.manager_id))

    def remove_manager(self, manager_id):
        """
        removes manager from application by id
        :param manager_id:
        :return None:
        """
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("searching for manager " + str(manager_id))
            for i in range(len(self.managers)):
                if self.managers[i].manager_id == manager_id:
                    del self.managers[i]
                    if self.selected_manager and self.selected_manager.manager_id == manager_id:
                        self.selected_manager = None
                    logger.info("deleted manager " + str(manager_id))
                    return
            else:
                raise InputError("manager " + str(manager_id) + " does not exist")

    def select_manager(self, manager_id: int):
        """
        selects manager in application by id
        :param manager_id:
        :return:
        """
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("searching for manager " + str(manager_id))
            for man in self.managers:
                if man.manager_id == manager_id:
                    for user in man.user_list:
                        if user.user_id == self.selected_user.user_id:
                            self.selected_manager = man
                            return
                    raise InputError("selected user is not invited to this manager")

            logger.error("not found manager" + str(manager_id))
            raise InputError("this manager doesn't exist")

    def show_available_managers(self):
        return [man.manager_id for man in self.managers if self.selected_user in man.user_list]

    def add_task(self, task: tt.Task):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("adding Task " + str(task.task_id))
            sfuncs.add_task_to_dict(self.selected_manager, task)
            self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager
            logger.info("Task added " + str(task.task_id))

    def remove_task(self, task_id: int):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("searching for Task " + str(task_id))
            removed = sfuncs.remove_task_from_manager(self.selected_manager, task_id)
            self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager
            if removed:
                logger.info("deleted task " + str(task_id))
                return "task removed"
            else:
                raise InputError("task not found")

    def move_task(self, task_id, new_date):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("searching for task " + str(task_id))
            sfuncs.move_task(self.selected_manager, task_id, new_date)
            logger.info("moved to a new date task " + str(task_id))

    def change_parent(self, child_id, parent_id):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("finding child task " + str(child_id))
            child = self.find_task(child_id)
            if child is None:
                raise ValueError("child is None")
            logger.info("child found")

            logger.info("finding parent task " + str(parent_id))
            parent = self.find_task(parent_id)
            if parent is None:
                raise ValueError("parent is None")
            logger.info("parent found")

            logger.info("removing child task " + str(child_id))
            self.remove_task(child_id)
            logger.info("child removed")

            self.add_sub_task(child, parent_id)

    def find_task(self, task_id: int):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("searching task " + str(task_id))
            task = sfuncs.find_task_in_manager(self.selected_manager, task_id)
            if task:
                return task
            else:
                raise InputError("task not found")

    def show_task(self, task_id: int):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("searching task " + str(task_id))
            task = sfuncs.find_task_in_manager(self.selected_manager, task_id)
            if task:
                tfuncs.print_task(task)
            else:
                raise InputError("task not found")

    def set_archive(self, task_id: int):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("searching task " + str(task_id))
            task = sfuncs.find_task_in_manager(self.selected_manager, task_id)
            if task:
                task.archive = True
                self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager
            else:
                raise InputError("task not found")

    def complete_task(self, task_id: int):
        task = sfuncs.find_task_in_manager(self.selected_manager, task_id)
        if task:
            tfuncs.set_state(task, tt.State.COMPLETED)
            self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager

    def add_sub_task(self, task: tt.Task, pid: int):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            parent = sfuncs.find_task_in_manager(self.selected_manager, pid)
            if parent is None:
                raise InputError("parent task not found")
            else:
                task.last_date = parent.last_date
                task.state = parent.state
                parent.sub_tasks.append(task)
                self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager

    def create_user(self):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("creating new user")
            usr = user.User()
            self.users.append(usr)
            self.selected_user = self.users[-1]
            if self.selected_manager and self.selected_user not in self.selected_manager.user_list:
                self.selected_manager = None

    def change_task(self, task_id: int, name=None, description=None, priority=None):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            found = sfuncs.find_task_in_manager(self.selected_manager, task_id)
            if found:
                if name:
                    found.name = name
                if description:
                    found.description = description
                if priority:
                    found.priority = priority
            self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager

    def show_all_users(self):
        return [usr for usr in self.users]

    def select_user(self, user_id: int):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            for usr in self.users:
                if usr.user_id == user_id:
                    self.selected_user = usr
                    if not (self.selected_manager is None) and not (self.selected_user in self.selected_manager.user_list):
                        self.selected_manager = None
                    return
            else:
                raise InputError("user not found")

    def remove_user(self, user_id: int):
        for i in range(len(self.users)):
            if self.users[i].user_id == user_id:
                del self.users[i]
                if self.selected_user.user_id == user_id:
                    self.selected_user = None
                return

    def invite_user(self, user_id):
        """
        adds a new user to manager
        :param user_id:
        :return:
        """
        with ContextLogger(self.logging_file, self.do_log) as logger:
            if self.selected_manager is None:
                raise InputError("manager is not selected. select or create it first")
            invited_user = None
            for usr in self.users:
                if usr.user_id == user_id:
                    invited_user = usr
                    break
            else:
                raise InputError("no such an user")

            for usr in self.selected_manager.user_list:
                if usr.user_id == invited_user.user_id:
                    raise InputError("user already has access")
            self.selected_manager.user_list.append(invited_user)
            self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager

    def drive_user_away(self, user_id):
        """
        removes user(if not owner) from manager
        :param user_id:
        :return:
        """
        user_list = self.selected_manager.user_list
        for i in range(len(user_list)):
            if user_list[i].user_id == user_id and i != 0:  # i != 0 means not owner
                del user_list[i]
                self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager
                if self.selected_user.user_id == user_id:
                    self.selected_manager = None
                return
            elif user_list[i].user_id == user_id and i == 0 and self.selected_user.user_id == user_id:
                raise InputError("you can't remove owner from the task")
        else:
            raise InputError("user not found")

    def plan_task(self, task: tt.Task, years=0, months=0, days=0, weekdays=None):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("planning new task" + str(task.task_id))
            sfuncs.plan_task(task, years, months, days, weekdays)
            self.add_task(task)
            self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager
            logger.info("set to be replanned task " + str(task.task_id))

    def replan_tasks(self):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            logger.info("replanning all tasks for today")
            if self.selected_manager is None:
                return
            keys = tuple(self.selected_manager.task_dict.keys())
            for day in keys:
                for task in self.selected_manager.task_dict[day]:
                    if task.is_planned and (task.state == tt.State.OUT_OF_TIME or task.state == tt.State.COMPLETED):
                        sfuncs.replan_task(self.selected_manager, task)
                        task.is_planned = False
                        self.selected_manager.task_dict[day][self.selected_manager.task_dict[day].index(task)] = task
            self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager
            logger.info("tasks are replanned")

    def cancel_plan_task(self, task_id: int):
        found_task = sfuncs.find_task_in_manager(self.selected_manager, task_id)
        if found_task:
            found_task.is_planned = False
            found_task.planned_from = None
        self.managers[self.managers.index(self.selected_manager.manager_id)] = self.selected_manager

    def show_selected_user(self):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            if self.selected_user is None:
                raise InputError("user is not selected create it first to be able to manage the tasks")
            else:
                return "you are " + str(self.selected_user.user_id)

    def show_all_tasks(self):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            if self.selected_manager is None:
                raise InputError("manager doesn't exist")
            keys = list(self.selected_manager.task_dict.keys())
            keys.sort()
            for day in keys:
                for task in self.selected_manager.task_dict[day]:
                    if not task.archive:
                        tfuncs.print_task(task)

    def show_archive(self):
        with ContextLogger(self.logging_file, self.do_log) as logger:
            if self.selected_manager is None:
                raise InputError("manager don't exist")
            keys = list(self.selected_manager.task_dict.keys())
            keys.sort()
            for day in keys:
                for task in self.selected_manager.task_dict[day]:
                    if task.archive:
                        tfuncs.print_task(task)

    def show_my_tasks(self):
        for day in self.selected_manager.task_dict.values():
            for task in day:
                if task.user_id == self.selected_user.user_id:
                    tfuncs.print_task(task)

    def show_usr_tasks(self, user_id):
        for day in self.selected_manager.task_dict.values():
            for task in day:
                if task.user_id == self.selected_user.user_id:
                    tfuncs.print_task(task)

    def check_overdued_tasks(self):
        sfuncs.check_days_overdue(self.selected_manager)

    def app_status(self):
        ret_str = ""
        if self.selected_manager is None:
            ret_str += "manager is not selected"
        else:
            ret_str += "selected manager " + str(self.selected_manager.manager_id)

        ret_str += "\n"

        if self.selected_user is None:
            ret_str += "user is not selected"
        else:
            ret_str += "selected user is " + str(self.selected_user.user_id)
        self.check_overdued_tasks()
        return ret_str


def pickle_app(appl: Application, cfg: configparser.ConfigParser, run_type):
    user_file = os.path.join(str(pathlib.Path.home()), cfg[run_type]["users_file"])
    manager_file = os.path.join(str(pathlib.Path.home()), cfg[run_type]["managers_file"])
    sel_usr_f = os.path.join(str(pathlib.Path.home()), cfg[run_type]["selected_user"])
    sel_man_f = os.path.join(str(pathlib.Path.home()), cfg[run_type]["selected_manager"])

    if user_file is None or manager_file is None or sel_usr_f is None or sel_man_f is None:
        raise SystemError("fatal error while trying to locate home directory")

    with open(user_file, "wb") as f:
        pickle.dump(appl.users, f)
    with open(manager_file, "wb") as f:
        pickle.dump(appl.managers, f)
    with open(sel_usr_f, "wb") as f:
        pickle.dump(appl.selected_user, f)
    with open(sel_man_f, "wb") as f:
        pickle.dump(appl.selected_manager, f)


def unpickle_app(cfg: configparser.ConfigParser, run_type):
    user_file = os.path.join(str(pathlib.Path.home()), cfg[run_type]["users_file"])
    manager_file = os.path.join(str(pathlib.Path.home()), cfg[run_type]["managers_file"])
    sel_usr_f = os.path.join(str(pathlib.Path.home()), cfg[run_type]["selected_user"])
    sel_man_f = os.path.join(str(pathlib.Path.home()), cfg[run_type]["selected_manager"])
    try:
        f = open(user_file, "rb")
        f.close()
        f = open(manager_file, "rb")
        f.close()
        f = open(sel_usr_f, "rb")
        f.close()
        f = open(sel_man_f, "rb")
        f.close()
    except FileNotFoundError:
        return Application(cfg, run_type)

    appl = Application(cfg, run_type)
    with open(user_file, "rb") as f:
        users = pickle.load(f)
        appl.users = users
    with open(manager_file, "rb") as f:
        managers = pickle.load(f)
        appl.managers = managers
    with open(sel_usr_f, "rb") as f:
        state_user = None
        try:
            state_user = pickle.load(f)
        except EOFError:
            # it's normal not to have selected user
            pass
        appl.selected_user = state_user
    with open(sel_man_f, "rb") as f:
        state_man = None
        try:
            state_man = pickle.load(f)
        except EOFError:
            # second normal state not to have a selected manager
            pass
        appl.selected_manager = state_man

    appl.count_managers()
    appl.count_tasks()
    appl.count_users()
    appl.check_overdued_tasks()
    appl.replan_tasks()
    return appl


def jsonize_app(appl: Application, cfg: configparser.ConfigParser, run_type: str):
    user_file = os.path.join(str(pathlib.Path.home()), cfg[run_type]["users_file_j"])
    manager_file = os.path.join(str(pathlib.Path.home()), cfg[run_type]["managers_file_j"])
    sel_usr_f = os.path.join(str(pathlib.Path.home()), cfg[run_type]["selected_user_j"])
    sel_man_f = os.path.join(str(pathlib.Path.home()), cfg[run_type]["selected_manager_j"])

    if user_file is None or manager_file is None or sel_usr_f is None or sel_man_f is None:
        raise SystemError("fatal error while trying to locate home directory")

    with open(sel_usr_f, "w") as f:
        if appl.selected_user:
            usr = {"user_id": appl.selected_user.user_id}
            json.dump(usr, f, indent=4)

    with open(sel_man_f, "w") as f:
        if appl.selected_manager:
            manager = {"manager_id": appl.selected_manager.manager_id}
            json.dump(manager, f, indent=4)

    with open(user_file, "w") as f:
        users = []
        for usr in appl.users:
            users.append(usr.user_id)
        json.dump(users, f, indent=4)


    with open(manager_file, "w") as f:
        managers = {}
        for manager in appl.managers:
            managers[manager.manager_id] = {}
            users = []
            for usr in manager.user_list:
                users.append(usr.user_id)
            managers[manager.manager_id]["users"] = users
            days = {}
            for day in manager.task_dict:
                days[str(day)] = []
                for task in manager.task_dict[day]:
                    task_fields = {}
                    task_fields["name"] = task.name
                    task_fields["description"] = task.description
                    task_fields["last_date"] = str(task.last_date)
                    task_fields["comment"] = task.toad_comment
                    task_fields["priority"] = task.priority
                    task_fields["user_id"] = task.user_id
                    task_fields["planned_from"] = task.planned_from
                    task_fields["replan_period"] = str(task.replan_period)
                    task_fields["task_id"] = task.task_id
                    days[str(day)].append(task_fields)
            managers[manager.manager_id]["days"] = days
        json.dump(managers, f, indent=4)


def unjsonize_app(cfg: configparser.ConfigParser, run_type: str):
    user_file = os.path.join(str(pathlib.Path.home()), cfg[run_type]["users_file_j"])
    manager_file = os.path.join(str(pathlib.Path.home()), cfg[run_type]["managers_file_j"])
    sel_usr_f = os.path.join(str(pathlib.Path.home()), cfg[run_type]["selected_user_j"])
    sel_man_f = os.path.join(str(pathlib.Path.home()), cfg[run_type]["selected_manager_j"])

    try:
        f = open(user_file)
        f.close()
        f = open(manager_file)
        f.close()
        f = open(sel_usr_f)
        f.close()
        f = open(sel_man_f)
        f.close()
    except FileNotFoundError:
        return Application(cfg, run_type)

    appl = Application(cfg, run_type)
    with open(user_file, "r") as f:
        users = json.load(f)
        loaded = []
        for usr in users:
            l_usr = user.User()
            l_usr.user_id = usr
            loaded.append(l_usr)
        appl.users = loaded

    with open(manager_file, "r") as f:
        managers = json.load(f)
        for man in managers:
            usrs = []
            for usr_id in managers[man]["users"]:
                l_usr = user.User()
                l_usr.user_id = usr_id
                usrs.append(l_usr)
            l_man = scheduling.Manager(usrs)
            l_man.manager_id = int(man)
            days = managers[man]["days"]
            for day in days:
                for task in days[day]:
                    string_time = task["last_date"]
                    ld = parser.parse(string_time)
                    key = parser.parse(day)
                    if not (day in l_man.task_dict):
                        l_man.task_dict[key] = []
                    l_task = tt.Task(task["name"], task["description"], ld, task["comment"],
                                   task["priority"], task["user_id"])
                    l_task.task_id = task["task_id"]
                    l_man.task_dict[key].append(l_task)
            appl.managers.append(l_man)

    with open(sel_usr_f, "r") as f:
        state_user = None
        try:
            usr = json.load(f)
            state_user = usr["user_id"]
            appl.select_user(state_user)
        except EOFError:
            # it's normal not to have selected user
            pass

    with open(sel_man_f, "r") as f:
        state_man = None
        try:
            man = json.load(f)
            state_man = man["manager_id"]
            appl.select_manager(state_man)
        except EOFError:
            # second normal state not to have a selected manager
            pass

    appl.count_managers()
    appl.count_tasks()
    appl.count_users()
    appl.check_overdued_tasks()
    appl.replan_tasks()
    return appl
