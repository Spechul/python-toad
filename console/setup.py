from setuptools import setup, find_packages
from os import mkdir
from os.path import join, dirname
from shutil import copyfile
import sys


def config_import():
    try:
        print("importing default config file")
        import pathlib
        bin_path = join(pathlib.Path.home(), "binaries")
        mkdir(bin_path)
        copyfile("configuration.ini", join(bin_path, "configuration.ini"))
        print("default config imported")
    except FileExistsError:
        copyfile("configuration.ini", join(bin_path, "configuration.ini"))
        print("default config imported")
    except:
        raise SystemError("cannot copy a configuration file try reinstalling the app to launch it")


# setup itself
setup(
    name="puthon-toad",
    version='1.0',
    packages=find_packages(),
    long_description=open(join(dirname(__file__), "README.md")).read(),
    install_requires=[
        "python-dateutil>=2.6.1",
        "configparser>=3.5.0",
        "pytz",
    ],
    entry_points={
       "console_scripts": [
           "ta = console_interface.ta:main",
           ],
       },
    test_suite='tests.tasktest'
)
if 'test' not in sys.argv:
    config_import()
