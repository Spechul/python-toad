### Лабораторные ИСП-2018  / IMP Labs-2018

# WARNING!: if you want to use this code in your purposes, note: it ain't have any support, so i won't be answering to issues


### This repo contains of 2 applications to track tasks (just for as if you want to mage some notes and then track, what have you done and what haven't you done) with a library of some useful functions

### requires
- jsonpickle 0.9.6
- python-dateutil 2.6.1
- django 1.11.13
- configparser 3.5

> Also you may just use something like  this. I suppose i didn't miss this.
For django and for console there are different requirements, so be carefull
(requirements.txt files are in console and django folders)   

    python -m pip install -r requirements.txt
    
> Also if you pull this to use django, make sure, you applied migrations by typing in console

    python manage.py makemigrations

> and then
    python manage.py migrate

### command meaning for console (i hope you won't be using it as a console application)

#### addt
- -tt <big:el:sub> selects type for task.
- -n <name> selects name for new task.
- -d <description> selects description for new task.
- -ld <DD:MM:YYYY> selects last completion date for new task.
- -lt <HH:MM> selects last completion time for new task.
- -p <priority> selects priority for new task.
- -t <comment> selects comment for new task.

#### adds
- -n <name> selects name for new task.
- -d <description> selects description for new task.
- -ld <DD:MM:YYYY> selects last completion date for new task.
- -lt <HH:MM> selects last completion time for new task.
- -p <priority> selects priority for new task.
- -t <comment> selects comment for new task.
- -pid <parent identifier> selects parent big task to add to.

#### adde
- -n <name> selects name for new task.
- -d <description> selects description for new task.
- -ld <DD:MM:YYYY> selects last completion date for new task.
- -lt <HH:MM> selects last completion time for new task.
- -p <priority> selects priority for new task.
- -t <comment> selects comment for new task.
- -pid <parent identifier> selects parent sub task to add to.

#### mvtask
- -id <identifier> selects task identifier
- -d <DD.MM.YYYY> selects new day to move to
- -t <HH:MM> selects new time to move to. if not selected will be set to 00:00

#### plan
- -tt <big:el:sub> selects type for task.
- -n <name> selects name for new task.
- -d <description> selects description for new task.
- -ld <DD:MM:YYYY> selects last completion date for new task.
- -lt <HH:MM> selects last completion time for new task.
- -p <priority> selects priority for new task.
- -t <comment> selects comment for new task.
- -sd <DD.MM.YYYY> selects replan date

#### planwd
- -tt <big:el:sub> selects type for task.
- -n <name> selects name for new task.
- -d <description> selects description for new task.
- -ld <DD:MM:YYYY> selects last completion date for new task.
- -lt <HH:MM> selects last completion time for new task.
- -p <priority> selects priority for new task.
- -t <comment> selects comment for new task.
- -wd <int,int,int...> selects days to plan to

#### rmtask 
- -id <identifier> selects task identifier to find and delete by.

#### chtask
- -id <identifier> selects task identifier to find and change by 
- -n <name> selects new task name
- -d <description> selects new task description
- -p <priority> selects new task priority

#### addusr
- no arguments. 

#### selusr
- -id <identifier> selects task identifier to find and delete by.

#### whoami
- no arguments

#### invite
- -id <identifier> selects identifier to search by to find user.

#### driveaway
- -id <identifier> selects identifier to search by to find user. 

#### rmusr
- -id <identifier> selects identifier to search by to find user

#### showusrs
- no arguments

#### addman
- no arguments

#### selman 
- -id <identifier> selects identifier to search by to find manager

#### rmman
- -id <identifier> selects manager identifier to find and delete by.

#### showarc
- no parameters

#### setarc
- -id <identifier> selects identifier to search by to find task



