#! /home/asus-user/anaconda3/envs/ttenv/bin/python3.6

"""
module contains argument parser that receives command line arguments, converts them and
calls toad.app module functions. to run in DEBUG mode set the ENV variable "DEBUG" = "1"
"""

import configparser
from instances import tasktypes as tt
import datetime as dt
from toad import app
from instances.inputerror import InputError
import argparse
import os
import pathlib
import dateutil
import dateutil.parser
import dateutil.relativedelta as rd
import re


def create_task(application: app.Application, args):
    name = args.name
    description = args.description

    if application.selected_user is None:
        raise InputError("first you must select an user")

    if application.selected_manager is None:
        raise InputError("first you must select a manager")

    try:
        last_day = dateutil.parser.parse(args.lastday, dayfirst=True)
    except ValueError:
        last_day = None
    try:
        priority = int(args.priority)
    except ValueError:
        raise InputError("wrong input of priority")

    comment = args.comment
    task = tt.Task(name, description, last_day, comment, priority, application.selected_user.user_id)
    return task


def add_task(application: app.Application, args):
    task = create_task(application, args)
    application.add_task(task)
    print("task added")


def add_sub_task(application: app.Application, args):
    task = create_task(application, args)
    try:
        parent_id = int(args.parentid)
    except ValueError:
        raise InputError("wrong input of parent id")
    application.add_sub_task(task, parent_id)
    print("sub task added")


def create_manager(application: app.Application, args):
    application.create_manager()


def select_manager(application: app.Application, args):
    try:
        manager_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of manager id")

    application.select_manager(manager_id)


def remove_manager(application: app.Application, args):
    manager_id = int(args.identifier)
    application.remove_manager(manager_id)


def move_task(application: app.Application, args):
    try:
        task_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of task id")

    try:
        date = dateutil.parser.parse(args.date, dayfirst=True)
    except (TypeError, ValueError, AttributeError):
        raise InputError("wrong input of date")
    application.move_task(task_id, date)


def change_parent_task(application: app.Application, args):
    try:
        child_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of target task id")

    try:
        parent_id = int(args.parentid)
    except ValueError:
        raise InputError("wrong input of parent task id")

    application.change_parent(child_id, parent_id)


def change_task(application: app.Application, args):
    name = args.name
    desc = args.description
    try:
        priority = int(args.priority)
    except ValueError:
        raise InputError("wrong input of priority")

    try:
        task_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of task id")
    application.change_task(task_id, name, desc, priority)


def complete_task(application: app.Application, args):
    try:
        task_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of task id")
    application.complete_task(task_id)


def remove_task(application: app.Application, args):
    try:
        task_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of task id")
    application.remove_task(task_id)


def plan_task(application: app.Application, args):
    task = create_task(application, args)

    span_date = None

    span_date = re.split(r"[\.-]", args.spand)

    try:
        # ain't work with 01.00.0000 if i want to repeat every day
        # span_date = dateutil.parser.parse('01-00-0000', dayfirst=True, fuzzy=True)
        int(span_date[0])
        int(span_date[1])
        int(span_date[2])
    except (ValueError, KeyError):
        raise InputError("wrong span date was entered")

    if span_date:
        application.plan_task(task, days=int(span_date[0]), months=int(span_date[1]), years=int(span_date[2]))
    else:
        print("check your input, something wrong was entered for days or date span")


def plan_task_by_weekday(application: app.Application, args):
    task = create_task(application, args)

    wdays = []
    for day in args.weekdays.split(","):
        # print(day)
        wdays.append(int(day))
    application.plan_task(task, weekdays=wdays)


def change_task_parent(application: app.Application, args):
    child_id = int(args.identifier)
    parent_id = int(args.parentid)
    if child_id == parent_id:
        raise InputError("child and parent must be different tasks")

    application.change_parent(child_id, parent_id)


def add_user(application: app.Application, args):
    application.create_user()


def select_user(application: app.Application, args):
    try:
        user_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of user id")

    application.select_user(user_id)


def remove_user(application: app.Application, args):
    try:
        user_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of user id")

    application.remove_user(user_id)


def set_archive(application: app.Application, args):
    try:
        task_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of task id")

    application.set_archive(task_id)


def invite_user(application: app.Application, args):
    try:
        user_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of user id")

    application.invite_user(user_id)


def drive_away(application: app.Application, args):
    try:
        user_id = int(args.identifier)
    except ValueError:
        raise InputError("wrong input of user id")

    application.drive_user_away(user_id)


def whoami(application: app.Application, args):
    print(application.show_selected_user())


def show_all_users(application: app.Application, args):
    users = application.show_all_users()
    for usr in users:
        print(usr)


def show_archived(application: app.Application, args):
    application.show_archive()


def show_all_tasks(application: app.Application, args):
    application.show_all_tasks()


def show_my_tasks(application: app.Application, args):
    if application.selected_user:
        application.show_my_tasks()
    else:
        raise InputError("first you must create an user")


def show_usr_tasks(application: app.Application, args):
    application.show_usr_tasks(int(args.identifier))


def parse_arguments(application: app.Application):
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help="parsers for more complicated commands")

    # add parser
    add_parser = subparsers.add_parser("addt", help="add an elementary task")
    add_parser.add_argument("-n", "--name", help="task name", default="test task", required=True)
    add_parser.add_argument("-d", "--description", help="task description", default="description")
    add_parser.add_argument("-ld", "--lastday", help="last day to complete. format DD.MM.YYYY",
                            default="14.05.2018", required=True)
    add_parser.add_argument("-lt", "--lasttime", help="last time to complete. format HH:MM",
                            default="00:00")
    add_parser.add_argument("-p", "--priority", help="task priority", default=0)
    add_parser.add_argument("-t", "--comment", help="toad's comment", default="")
    add_parser.set_defaults(func=add_task)

    # add sub parser
    add_sub_parser = subparsers.add_parser("adds", help="add a subtask")
    add_sub_parser.add_argument("-n", "--name", help="task name", default="test task", required=True)
    add_sub_parser.add_argument("-d", "--description", help="task description", default="description")
    add_sub_parser.add_argument("-p", "--priority", help="task priority", default=0)
    add_sub_parser.add_argument("-t", "--comment", help="toad's comment", default="")
    add_sub_parser.add_argument("-pid", "--parentid", help="parent task", required=True)
    add_sub_parser.set_defaults(func=add_sub_task)

    # set archive parser
    set_archive_parser = subparsers.add_parser("setarc", help="sets task to archive")
    set_archive_parser.add_argument("-id", "--identifier", help="task identifier")
    set_archive_parser.set_defaults(func=set_archive)

    # change task parser
    change_task_parser = subparsers.add_parser("chtask", help="changes task description and name")
    change_task_parser.add_argument("-id", "--identifier", help="task identifier", required=True)
    change_task_parser.add_argument("-n", "--name", help="new name for task", default=None)
    change_task_parser.add_argument("-d", "--description", help="new description for task", default=None)
    change_task_parser.add_argument("-p", "--priority", help="new task priority")
    change_task_parser.set_defaults(func=change_task)

    # remove task parser
    remove_task_from_day_parser = subparsers.add_parser("rmtask", help="delete task from specified day")
    remove_task_from_day_parser.add_argument("-id", "--identifier", help="task identifier", default=1, required=True)
    remove_task_from_day_parser.set_defaults(func=remove_task)

    # add manager parser
    add_manager_parser = subparsers.add_parser("addman", help="adds new manager")
    add_manager_parser.set_defaults(func=create_manager)

    # select manager parser
    select_manager_parser = subparsers.add_parser("selman", help="selects manager")
    select_manager_parser.add_argument("-id", "--identifier", help="manager's identifier")
    select_manager_parser.set_defaults(func=select_manager)

    # remove manager parser
    remove_manager_parser = subparsers.add_parser("rmman", help="removes manager")
    remove_manager_parser.add_argument("-id", "--identifier", help="manager's identifier", required=True)
    remove_manager_parser.set_defaults(func=remove_manager)

    # move task to day parser
    move_task_to_day_parser = subparsers.add_parser("mvtask", help="move task to another day")
    move_task_to_day_parser.add_argument("-id", "--identifier", help="task identifier", default=1, required=True)
    move_task_to_day_parser.add_argument("-d", "--date", help="date to move to", default="14.05.2018", required=True)
    move_task_to_day_parser.set_defaults(func=move_task)

    # change parent task parser
    change_parent_task_parser = subparsers.add_parser("chparent", help="make task сhild of another one")
    change_parent_task_parser.add_argument("-id", "--identifier", required=True)
    change_parent_task_parser.add_argument("-pid", "--parentid", required=True)
    change_parent_task_parser.set_defaults(func=change_parent_task)

    # plan task parser
    plan_task_parser = subparsers.add_parser("plan", help="plan task for period of time. "
                                                          "be careful with the last days of the month when planning"
                                                               "by month")
    plan_task_parser.add_argument("-n", "--name", help="task name", default="test task", required=True)
    plan_task_parser.add_argument("-d", "--description", help="task description", default="description")
    plan_task_parser.add_argument("-ld", "--lastday", help="last day to complete. format DD.MM.YYYY",
                                  required=True)
    plan_task_parser.add_argument("-p", "--priority", help="task priority", default=0)
    plan_task_parser.add_argument("-t", "--comment", help="toad's comment", default="")
    plan_task_parser.add_argument("-sd", "--spand", help="task date", default="14.05.2018", required=True)
    plan_task_parser.set_defaults(func=plan_task)

    # plan weekday task parser
    plan_task_wd_parser = subparsers.add_parser("planwd", help="plan task for period of time")
    plan_task_wd_parser.add_argument("-n", "--name", help="task name", default="test task", required=True)
    plan_task_wd_parser.add_argument("-d", "--description", help="task description", default="description")
    plan_task_wd_parser.add_argument("-ld", "--lastday", help="last day to complete. format DD.MM.YYYY",
                                  default="14.05.2018", required=True)
    plan_task_wd_parser.add_argument("-p", "--priority", help="task priority", default=0)
    plan_task_wd_parser.add_argument("-t", "--comment", help="toad's comment", default="")
    plan_task_wd_parser.add_argument("-wd", "--weekdays", required=True)
    plan_task_wd_parser.set_defaults(func=plan_task_by_weekday)

    # complete task parser
    complete_task_parser = subparsers.add_parser("complete", help="set task to complete")
    complete_task_parser.add_argument("-id", "--identifier", help="task identifier", required=True)
    complete_task_parser.set_defaults(func=complete_task)

    # show all tasks parser
    show_all_tasks_parser = subparsers.add_parser("showall", help="shows all tasks")
    show_all_tasks_parser.set_defaults(func=show_all_tasks)

    # show archived tasks parser
    show_archived_parser = subparsers.add_parser("showarc", help="shows archived tasks")
    show_archived_parser.set_defaults(func=show_archived)

    # show my tasks parser
    show_my_tasks_parser = subparsers.add_parser("showmy", help="shows current user's tasks")
    show_my_tasks_parser.set_defaults(func=show_my_tasks, help="shows current user's tasks")

    # add user parser
    add_user_parser = subparsers.add_parser("addusr", help="adds new user")
    add_user_parser.set_defaults(func=add_user)

    # show all users parser
    show_all_users_parser = subparsers.add_parser("showusrs", help="shows all existing users")
    show_all_users_parser.set_defaults(func=show_all_users)

    # invite user parser
    invite_user_parser = subparsers.add_parser("invite", help="invites user to selected manager")
    invite_user_parser.add_argument("-id", "--identifier", help="user identifier", required=True)
    invite_user_parser.set_defaults(func=invite_user)

    # drive user away parser
    drive_away_parser = subparsers.add_parser("driveaway", help="drives user away from manager")
    drive_away_parser.add_argument("-id", "--identifier", help="user identifier", required=True)
    drive_away_parser.set_defaults(func=drive_away)

    # who am i parser
    whoami_parser = subparsers.add_parser("whoami", help="shows current user")
    whoami_parser.set_defaults(func=whoami)

    # remove user parser
    remove_user_parser = subparsers.add_parser("rmusr", help="removes user")
    remove_user_parser.add_argument("-id", "--identifier", help="user identifier", required=True)
    remove_user_parser.set_defaults(func=remove_user)

    # select user parser
    select_user_parser = subparsers.add_parser("selusr", help="selects user by id")
    select_user_parser.add_argument("-id", "--identifier", help="user identifier", required=True)
    select_user_parser.set_defaults(func=select_user)

    try:
        args = parser.parse_args()
        if "func" in args:
            args.func(application, args)

    except InputError as e:
        print(str(e))
        return


def main():
    run_type = "DEFAULT"
    if "DEBUG" in os.environ.keys() and os.environ["DEBUG"] == "1":
        # print("ololo, it's debug time")
        run_type = "DEBUG"
    try:
        os.mkdir(os.path.join(str(pathlib.Path.home()), "binaries"))
    except FileExistsError:
        pass

    cfg = configparser.ConfigParser()
    cfg.read(os.path.join(str(pathlib.Path.home()), "binaries/configuration.ini"))

    application = app.unpickle_app(cfg, run_type)
    parse_arguments(application)
    print(application.app_status())

    app.pickle_app(application, cfg, run_type)


if __name__ == '__main__':
    main()
