"""
module contains functions that interact mainly with classes from toad.tt and toad.scheduling.
provides primary functionality
"""

import datetime as dt
from copy import deepcopy
import instances.tasktypes as tt
import dateutil.relativedelta as rd
import functions.taskfunctions as tfunc
import instances.scheduling as scheduling
import pytz


def copy_task(task: tt.Task):
    """
    in fact returns a deep copy of received object
    :param task:
    """
    return deepcopy(task)


def check_task_overdue(task: tt.Task):
    """
    checking task for overdue. :returns False if no overdue
                               :returns True if overdue
    :param task: Task
    :return: bool
    """
    if task.last_date < dt.datetime.today():
        task.state = tt.State.OUT_OF_TIME
        return True
    return False


def add_task_to_dict(manager: scheduling.Manager, task: tt.Task):
    """
    adds task to the day when it must be completed
    :param manager: Manager
    :param task: Task
    """
    date = None
    if isinstance(task.last_date, dt.date):
        date = task.last_date
    else:
        date = task.last_date.date()

    task.manager_id = manager.manager_id

    if date in manager.task_dict.keys():
        manager.task_dict[date].append(task)
        manager.task_dict[date].sort()
    else:
        manager.task_dict[date] = [task]


def check_days_overdue(manager: scheduling.Manager):
    """
    checks if tasks in received dictionary are overdued.
    also sets State.OUT_OF_TIME to them if they are
    :param manager:
    :return int:
    """
    if manager is None:
        return -1
    task_dict = manager.task_dict
    overdued_days = []
    today = dt.datetime.today()
    for key in task_dict.keys():
        if key <= today:
            overdued_days.append(key)
    overdued_tasks = 0
    for day in overdued_days:
        for task in task_dict[day]:
            if task.last_date.time() < today.time() and not (task.state == tt.State.COMPLETED):
                tfunc.set_state(task, tt.State.OUT_OF_TIME)
                overdued_tasks += 1

    return overdued_tasks


def find_task_in_day(manager: scheduling.Manager, task_date: dt.date, task_id: int):
    """
    finds task in manager's day by it's id
    :param manager:
    :param task_date:
    :param task_id:
    :return tt.Task:
    """
    found_task = None
    for task in manager.task_dict[task_date]:
        if task.task_id == task_id:
            found_task = task
    return found_task


def find_task_in_manager(manager: scheduling.Manager, task_id: int):
    """
    finds task by it's id in manager
    returns None if no task is not found
    :param manager:
    :param task_id:
    :return tt.Task:
    """
    found_task = None
    for ls in manager.task_dict.values():
        for task in ls:
            if task.task_id == task_id:
                found_task = task
                break
            if not found_task:
                found_task = tfunc.find_nested(task, task_id)
        if found_task:
            break

    return found_task


def move_task(manager: scheduling.Manager, task_id: int, new_date: dt.datetime, new_pid=-1):
    """
    moves selected by id task to selected date
    :param manager:
    :param task_id:
    :param new_date:
    """
    found = find_task_in_manager(manager, task_id)
    copied = copy_task(found)
    manager.task_dict[found.last_date.date()].remove(found)
    copied.last_date = new_date
    add_task_to_dict(manager, copied)


def remove_task_from_manager(manager: scheduling.Manager, task_id: int):
    """
    removes task from day by id
    :param manager:
    :param task_id:
    :param task_date:
    """
    deleted = False
    found = find_task_in_manager(manager, task_id)
    if not found:
        for day in manager.task_dict.keys():
            for task in manager.task_dict[day]:
                deleted = tfunc.delete_nested(task, task_id)
    if not deleted:
        try:
            manager.task_dict[found.last_date.date()].remove(found)
            deleted = True
        except:
            deleted = False
    return deleted


def count_all_tasks(managers: [scheduling.Manager]):
    """
    Counts tasks in all managers in list
    :param managers:
    :return int:
    """
    for mng in managers:
        for day in mng.task_dict.keys():
            for task in mng.task_dict[day]:
                tfunc.find_max_id(task)


def plan_task(task: tt.Task, years=0, months=0, days=0, weekdays=None):
    """
    Sets task to be replanable. Also sets date values span to it
    :param task:
    :param years:
    :param months:
    :param days:
    """
    task.is_planned = True
    if (years > 0 or months > 0 or days > 0) and weekdays is None:
        tfunc.set_state(task, tt.State.IN_PROGRESS)
        task.replan_period = rd.relativedelta(years=years, months=months, days=days)
    elif weekdays:
        for weekday in weekdays:
            if weekday not in task.replan_weekdays:
                task.replan_weekdays.append(weekday)
                tfunc.set_state(task, tt.State.IN_PROGRESS)
    else:
        task.is_planned = False


def generate_new_date(task: tt.Task, today: dt.datetime, day: int):
    last_hour = task.last_date.hour
    last_minute = task.last_date.minute
    weekday_now = today.isoweekday()
    new_last_date = None
    if day > weekday_now:
        new_last_date = today + rd.relativedelta(days=day - weekday_now) + \
                        rd.relativedelta(hours=last_hour, minutes=last_minute)
    else:
        new_last_date = today + rd.relativedelta(days=(7 - weekday_now + day)) + \
                        rd.relativedelta(hour=last_hour, minute=last_minute)
    return new_last_date


def replan_task(manager: scheduling.Manager, task: tt.Task):
    """
    replans task to a new date span or to nearest week. it's possible to plan less tasks than was asked
    because of this. for example you ask to plan onto [1,3,5] and today is thursday. there would be only two new tasks
    :param manager:
    :param task:
    """
    def duplicate(task: tt.Task):
        new_task = copy_task(task)
        if new_task.planned_from is None:
            new_task.planned_from = task.task_id
        tfunc.set_state(new_task, tt.State.IN_PROGRESS)
        tfunc.shift_task_enumeration(new_task)
        new_task.is_planned = True
        return new_task

    def replan(task: tt.Task, man: scheduling.Manager, day: int, today: dt.datetime):
        new_last_date = generate_new_date(task, today, day)
        weekday_now = today.isoweekday()
        if new_last_date in man.task_dict.keys():
            for existed_task in man.task_dict[new_last_date]:
                if existed_task.planned_from == copied.planned_from:
                    return
        task.last_date = new_last_date
        add_task_to_dict(man, task)

    today = dt.datetime.today()
    if today >= task.last_date:
        if task.replan_weekdays and task.last_date <= today:
            for day in task.replan_weekdays:
                copied = duplicate(task)
                replan(copied, manager, day, today)

        elif task.replan_period:
            copied = duplicate(task)
            tfunc.set_last_date(copied, copied.last_date + copied.replan_period)

            add_task_to_dict(manager, copied)


def replan_task_only(task: tt.Task):
    """
    sets new last date to task according to replan period
    :param manager:
    :param task:
    """

    def replan(task: tt.Task, day: int, today: dt.datetime):
        new_last_date = generate_new_date(task, today, day)

        task.last_date = new_last_date
        tfunc.set_state(task, tt.State.IN_PROGRESS)

    today = dt.datetime.today()
    utc = pytz.UTC
    u_today = utc.localize(today)

    if task.replan_weekdays:
        for day in task.replan_weekdays:
            if day > task.last_date.isoweekday():
                replan(task, day, today)
                break
        else:
            replan(task, task.replan_weekdays[0], today)

    elif task.replan_period:
        tfunc.set_last_date(task, task.last_date + task.replan_period)
        tfunc.set_state(task, tt.State.IN_PROGRESS)


def cancel_plan_task(task: tt.Task):
    task.is_planned = False
