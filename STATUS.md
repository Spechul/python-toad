### g. Журнал записей прогресса лаб

- 03.03.18 сегодня была лекция. жду 2-ю лабораторную. пошёл дочитывать пепы из 1-й и смотреть, что такое Markdown формат.
- вечер того же дня. пробую поиграться с форматом. посмотрел как это делать по [первой попавшейся ссылке](http://paulradzkov.com/2014/markdown_cheatsheet/ "ссылка на пример"), чуть подкрасивил сам файл.
- 08.03.18 вчера поковыряли пару предложенных приложений: [Wunderlist](https://play.google.com/store/apps/details?id=com.wunderkinder.wunderlistandroid&hl=ru), [Todoist](https://play.google.com/store/apps/details?id=com.todoist), [School](https://play.google.com/store/apps/details?id=com.diy.school), сайтовую версию [any.do](https://web.any.do/). пока что больше всех понравилась Todoist. относительно реализации - очень бы хотелось попробовать сделать деревом, но, как видно, проще зайдёт на списках. по поводу списков, наверное таки разные объекты для большого дела, подзадачи и элементарной работы. схема, которая у меня сейчас на уме, залита в репозиторий. если что - использовал [Violet UML Editor](http://alexdp.free.fr/violetumleditor/page.php "ссылка на скачивание").
- в данный момент, как видно по коммитам, я понемногу заполняю отчёт ко 2-й лабе.
- 10.03.18 вечер. сегодня была лекция. так, как на лекции было замечание - сегодня ещё раз обдумываю, что могло быть не учтено.
- 11.03.18 на данный момент всё ещё ждём 3-ю лабораторную. поковырял почти все темы лекций. (итераторы оказались почти такими же как в C# с парой отличий). посмотрел по лекции и немного дополнительно про кодировку Unicode и её "ответвления". также ознакомился с библиотекой unittest.
- 06.05.18 сейчас уже есть рабочие части трекера. часть откомментирована. пока осталось меню и тесты.

#### a. Аналоги:

изначально, конечно же были просмотрны некоторые приложения с таким функционалом, просто чтобы не потерять мысль, я сразу писал остальные пункты

- [School](https://play.google.com/store/apps/details?id=com.diy.school). для нашего случая - экземпляр средней помойности. нету задач (возможны только мелкие заметки), зато есть планировщик и чередующиеся недели (правда только 2, но имхо, своих потребителей оно покрывает). интерфейс вроде приятный.

- [Todoist](https://play.google.com/store/apps/details?id=com.todoist). в принципе, как и последующие 2 программы - почти то, что нужно. тут уже можно и задачи переносить. проблема только в том, что это дело за бесплатно работает только процентов на 30 и постоянно предлагает премиум.

- [Wunderlist](https://play.google.com/store/apps/details?id=com.wunderkinder.wunderlistandroid&hl=ru). что-то очень похожее на предыдущее, но только с бОльшим количеством функций, доступных бесплатно.

- [any.do](https://web.any.do/). по поводу этого у нас разошлись мнения (ковыряли вдвоём на перерыве между парами). как по мне, сильно any.do от этих 2-х приложений не отличается, но мой товарищ посчитал, что этой штукой пользоваться трудно и непонятно, как там что работает.  

Вообще, развивая тему моего товарища, я бы сказал, что во всех приложениях, кроме School нужно было понажимать кнопки и посмотреть, что они делают, чтобы понять, как они работают. по поводу недружелюбности к пользователю - у всех есть какие-то функции, которые замечаешь не сразу(к примеру перемещение задач в очереди Todoist, да оно там есть).

#### b. Сценарии использования:

1. Использование одним человеком.
2. Возможность планировать повторяющиеся события заранее.
3. Возможность отслеживать как простые события, так и составные.
4. Использование группой людей.
5. Обмен какой-либо дополнительной информацией, кроме заметок.

#### c. Планируемые возможности:

1. Создание разовой элементарной задачи с состоянием готов/не готов.
2. Создние небольшой составной задачи, частями которой будут элементарные задачи (и соответственно она сможет показывать свой прогресс относительно выполнения частей). это будет задача для 1 человека из следующего пункта.
3. Создание большой задачи для группы людей (задача максимум, т.к. тут ещё нужно будет придумывать что-то с аккаунтами и доступом к задачам).
4. Календарь-список, содержащий в себе дни и задачи, прикреплённые к ним.
5. Уже предложенный в тексте лабораторной планировщик задач. скорее всего будет для элементарных и составных задач. ну и конечно же его нужно будет делать на основе ?системного? календаря.
6. Задача должна будет иметь в себе описание, комментарий от жабки* (о том, довольна ли жабка прогрессом задачи) и как возможная платная (задача максимум - сделать ещё и микротранзакции) функция - добавление файлов.
7. *Жабка. персонаж, который будет представлять собой систему отслеживания задач, знать, какой сегодня день недели и в зависимости от этого предупреждать пользователя о том, что пора бы заняться чем-то (к примеру забирать инфу из планировщика и говорить "сегодня у нас по плану вот это, не забудь, напоминаю, потому, что ты это добавил достаточно давно"), комментировать какие либо задачи (оставлять в их описании напоминание/похвалу). если задача просрочена, жабка будет спрашивать о том, не забыл ли пользователь отметить эту задачу (эта возможность предлагалась на лекции).
8. *Документирование того, как пользователь называет свои задачи, чтобы по шаблону подсказывать для них название.
9. Перенос задачи из 1-й группы в другую (на одном уровне вложенности конечно).
10. Удаление любой задачи на любом уровне (без пометки, что она выполнена).
11. Сериализация всего этого дела.
12. Ограничение задач по времени.
13. Система аккаунтов для использования больше, чем одним человеком одновременно. (задача максимум)
14. Синхронизация действий пользователей.

(1-4, 6~)Если повезёт - реализуется всё, что написано. в пункте и. уже сказано про схему, того, как всё будет выглядеть. для представления будет 2 варианта: что-то похожее на дерево с n ветвями, в котором задачи разбиты до элементарных. так как в этом представлении удобно смотреть общий прогресс, но не хронологический порядок выполнения - будет отдельный список, представляющий собой очередь выполнения (в него соответственно будут входить ссылки на все задачи).  
(5)планировщик будет уметь задавать задачу на конкретный день, а также будет хранить в себе всё то, что пользователь назапланировал.  
(6~)По поводу прикрепления файлов - у задач будет точно такой же скисок файлов. я пока таки не уверен, как они будут отображаться в веб версии.  
(7)Про жабку, я думаю, уже достаточно сказано. только уже к веб версии сделаем так, чтобы она вылазила откуда-то сбоку и что-нибудь говорила, и давала ссылки на те задачи, которые запланированы на данный день. ну и конечно же ей будет нужна аватарка для того, чтобы размещать в этих сообщениях и комментариях к задачам [![Жабка](//placeold.it/100x100)](https://cs5.pikabu.ru/post_img/2014/12/13/12/1418502717_1446953895.webm).  
(8-12) документирование названий - просто запись их в файл/базу данных с последующими подсказками при вводе. перенос на 1 уровне - скорее всего в 1 функцию, зря я наверно его отдельно вынес.  
(13-14) сделать аккаунты и запихать их в базу - по идее ещё хватит времени, но делать личные кабинеты и возможность просмотра истории других пользователей - думаю будет сложновато успеть.(в том числе дополнительный дизайн, в котором я нулёвый).

#### d. Логическая архитектура:

1. Задачи. на данный момент план следующий: создаём класс Task, у которого будет только текст, прикреплённый файл и комментарий от жабки и галочка о выполнении. от Task наследуем SubTask, у которого появится прогресс выполнения в частях и список из Task-ов. от SubTask наследуем BigTask, у которого будет перегружена шкала выполнения так, чтобы показывать в процентах и список уже из SubTask-ов.
2. Группы задач. TaskGroups - это будет вместилище для всего, что приводится к Task. таким образом в 1 кипе мы сможем увидеть все варианты задач(если сильно надо - можно прикрутить фильтры).
3. День. Day вместилище для всего, что приводится к Task. также будет иметь доступ к календарю(это же день всё-таки)
7. Логгер. InfoLogger - класс, который будет записывать интересующие программу данные, которые потом можно будет использовать (уже по моему усмотрению для мониторинга к примеру).
4. Список насущных дел. CurrentJobsList - это будет список для Day. также все элементы в нём будут отсортированы в порядке: сначала те, у которых дата наступит раньше, потом те, у которых позже.
5. Планировщик. Sheduler - будет иметь доступ к подобию дерева TaskGroups(чтобы создать задачу) и к CurrentJobsList, чтобы добавить в нужный день задачу.
6. Жабка. Toad - содержит несколько списков фраз(похвалы, начальные, напоминания, гневные и т.д.), ссылки на все задачи(чтобы напоминать про конкретную задачу) и ссылку на Sheduller(мало ли нужна будет инфа про заплпанированные задачи)
8. Сериализатор. Serializer - вот тут не уверен на данный момент. скорее всего сделаю что-то похожее на ISerializable из C#. а вот делать класс или обойтись функцией - пока не уверен, но склоняюсь к классу, который будет иметь ссылки на всё и просто повызывает .Serialize у всех объектов, состояние которых нам важно сохранять.
9. база данных(я надеюсь, в ближайшем будущем разберусь с тем, что здесь конкретно доджно быть написано).
10. Аккаунты. скорее всего класс Account будет состоять из login'a и пароля. при авторизации будет происходить нехитрое сравнение, соответствует пвроль логину или нет.

#### e. Этапы разработки:

1-й этап. Для начала планируется покрыть все виды задач(большую только для 1 человека) и их взаимодействие в системе(1, 2, 3, 4).


2-й этап - добавление пунктов (8, 9, 10, 11, 12). на этом этапе у нас уже будет более-менее рабочая программа. далее перейдём к планировщику(5), прикрутим к нему календарь и спросим, как лучше сделать саму планировку (сразу забить весь календарь или добавлять задачу на неделю только если неделя началась). после этого создадим жабку(7) придумаем ей какие-нибудь прикольные фразы, скажем, через какое время жабка должна менять свой комментарий и укажем его содержание (начальный/похвала за выполнение части/напоминание о том, что уже крайний срок начала/гневное напоминание о том, что уже пора начинать/средняя оценка прогресса выполнения(быстро, нормально, медленно)) и дадим доступ к планировщику. также жабка будет писать в комментарии, если задача перенесена в другую группу.


3-й этап. прикручиваем ту базу данных, которая будет предложена. обустраиваем систему аккаунтов и прикрепляем её к базе данных.


4-й этап. Смотрим гайды по Django и пилим сайт. тут скорее всего возникнут определённые трудности (может даже что-то придётся порезать в проекте, чтобы всё стало на сайт, но будем надеяться, что не придётся).


#### f. Сроки:

Скажу честно, тут решаю не я, а расстановка пар в универе, поэтому очередной этап придётся выполнять к каждой паре(это в идеале). + сдают как правило со 2-го раза, поэтому надо будет ещё что-то уточнять. по итогу оптимистичная оценка такая: каждая следующая лабораторная сдаётся на очередной (если всё хорошо) или через пару (если есть косяки). в этом случае нужно 8 лабораторных занятий (если будет все 5 лабораторных и будем нормально успевать) или 6 лабораторных занятий (если будут проблемы и Сергей Игоревич пожмёт часть заданий). Далее я посмотрел в расписание и осознал, что у нас только 2 лабораторных на месяц (из-за клятых методистов и правил приёма 1 день - 1 лаба), а месяцев только март апрель и май. поэтому будем надеяться, что лабы всё-таки пожмут и получится всё доделать и сдать к концу мая.

