"""
module contains definition for class User
"""


class User:
    user_count = 0

    def __init__(self):
        User.user_count += 1
        self.user_id = User.user_count

    def __eq__(self, other):
        if self.user_id == other.user_id:
            return True
        else:
            return False
