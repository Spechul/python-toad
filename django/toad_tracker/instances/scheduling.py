"""
module contains definition for class Manager
"""


class Manager:
    """
    task manager. contains task_dict with tasks and user_list
    with users, invited in this manager. first user in list is owner
    """
    manager_count = 0

    def __init__(self, users: list, name=None):
        Manager.manager_count += 1
        self.name = name
        self.manager_id = Manager.manager_count
        self.task_dict = {}
        self.user_list = [usr for usr in users]

    @property
    def count(self):
        leng = 0
        for ls in self.task_dict.values():
            leng += len(ls)
        return leng

    def __str__(self):
        return str(self.name)

    def __eq__(self, other):
        return self.manager_id == other
