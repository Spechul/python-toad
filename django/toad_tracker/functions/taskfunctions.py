"""
module contains functions to interact with tasks
"""

import instances.tasktypes as tt
import datetime as dt


def set_priority(task: tt.Task, priority: int):
    task.priority = priority


def in_order(action):
    def perform_action(task, *args, **kwargs):
        obj = action(task, *args, **kwargs)
        if obj is not None:
            return obj
        if task.sub_tasks:
            for sub_t in task.sub_tasks:
                obj = perform_action(sub_t, *args, **kwargs)
                if obj is not None:
                    return obj

    return perform_action


@in_order
def set_state(task: tt.Task, state: tt.State):
    """
    sets state to task and it's children
    :param task:
    :param state:
    """
    task.state = state


@in_order
def find_nested(task: tt.Task, task_id: int):
    """
    finds task, using identifier
    :param task:
    :param task_id:
    """
    if task.task_id == task_id:
        return task


@in_order
def find_max_id(task: tt.Task):
    """
    finds maximal identifier in nested tasks
    :param task:
    """
    if task.task_id > tt.Task.task_count:
        tt.Task.task_count = task.task_id


@in_order
def set_last_date(task: tt.Task, date: dt.datetime):
    """
    sets new last date to task and its children
    :param task:
    :param date:
    """
    task.last_date = date


@in_order
def shift_task_enumeration(task: tt.Task):
    """
    shifts all identifiers in nested tasks (not quite by 1, but the way they wouldn't collide with copy)
    :param task:
    """
    tt.Task.task_count += 1
    task.task_id = tt.Task.task_count


@in_order
def delete_nested(task: tt.Task, task_id):
    """
    finds and deletes task in sent task
    :param task:
    :param task_id:
    """
    deleted = False
    if task.sub_tasks:
        for sub_t in task.sub_tasks:
            delete_nested(sub_t, task_id)
            if sub_t.task_id == task_id:
                del task.sub_tasks[task.sub_tasks.index(sub_t)]
                deleted = True
                return deleted


def print_task(task: tt.Task, indent=""):
    """
    performs pretty print of the task with nested tasks in it
    :param task:
    :param indent:
    """
    if task.sub_tasks:
        print(indent + str(task))
        for sub_t in task.sub_tasks:
            print_task(sub_t, indent + "\t")
    else:
        print(indent + str(task))
