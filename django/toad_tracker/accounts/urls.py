from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^sign-up/$', views.sign_up),
    url(r'^log-in/$', views.log_in),
    url(r'^log-out/$', views.log_out),
]