from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from .forms import CreateUserForm, LoginForm
from django.contrib.auth import login, logout


def sign_up(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/tasks/main')
    else:
        form = CreateUserForm()
    return render(request, 'accounts/signup.html', {'form': form})


def log_in(request):
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/tasks/main')
    else:
        form = LoginForm()
    return render(request, 'accounts/login.html', {'form': form})


def log_out(request):
    logout(request)
    return redirect('/accounts/log-in')
