# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-08-21 14:37
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0002_auto_20180821_1120'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subtask',
            name='status',
            field=models.CharField(choices=[('P', 'In Progress'), ('O', 'Overdue'), ('C', 'Completed')], default='P', max_length=1),
        ),
        migrations.AlterField(
            model_name='task',
            name='created',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 8, 21, 14, 37, 28, 593053)),
        ),
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.CharField(choices=[('P', 'In Progress'), ('O', 'Overdue'), ('C', 'Completed')], default='P', max_length=1),
        ),
    ]
