# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-08-25 09:08
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0006_auto_20180824_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='created',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 8, 25, 9, 8, 31, 777854)),
        ),
    ]
