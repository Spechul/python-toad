from django.db import models
from django.contrib.auth.models import User
from multiselectfield import MultiSelectField
from itertools import chain
from datetime import datetime
from instances.tasktypes import Task as LibTask, State as LibTaskState
from instances.scheduling import Manager as LibManager
from functions import schedulingfunctions as sfuncs
from dateutil.relativedelta import relativedelta
import ast


class TaskList(models.Model):
    name = models.CharField(max_length=64)
    users = models.ManyToManyField(User, related_name='task_lists', default=None, blank=True)
    owner = models.ForeignKey(
        User,
        related_name='created_lists',
        on_delete=models.CASCADE,
        default=None,
        null=True,
        blank=True
    )

    def __str__(self):
        return self.name


class Task(models.Model):
    PRIORITIES = (
        (2, 'High'),
        (1, 'Medium'),
        (0, 'Low'),
        (-1, 'None')
    )

    STATUS = (
        ('P', LibTaskState.IN_PROGRESS),
        ('O', LibTaskState.OUT_OF_TIME),
        ('C', LibTaskState.COMPLETED),
    )

    PERIODS = (
        ('D', 'Day'),
        ('W', 'Week'),
        ('N', 'None'),
    )

    DAYS = (
        (1, 'Monday'),
        (2, 'Tuesday'),
        (3, 'Wednesday'),
        (4, 'Thursday '),
        (5, 'Friday'),
        (6, 'Saturday'),
        (7, 'Sunday'),
    )

    title = models.CharField(max_length=200)
    description = models.TextField()
    created = models.DateTimeField(default=datetime.now(), blank=True)
    deadline = models.DateTimeField(default=None, blank=True, null=True)
    toad_comment = models.CharField(max_length=255, blank=True)
    created_user = models.ForeignKey(
        User,
        related_name='created_tasks',
        on_delete=models.CASCADE,
        default=None,
        null=True,
        blank=True)
    completed_user = models.ForeignKey(
        User,
        related_name='completed_tasks',
        on_delete=models.CASCADE,
        default=None,
        null=True,
        blank=True)
    parent = models.ForeignKey('Task', blank=True, on_delete=models.CASCADE, null=True)
    task_list = models.ForeignKey(TaskList, on_delete=models.CASCADE, default=None, null=True, blank=True)
    priority = models.IntegerField(choices=PRIORITIES, default=-1)
    status = models.CharField(max_length=1, choices=STATUS, default='P')
    repeat_days = MultiSelectField(max_length=10, max_choices=7, choices=DAYS, null=True, blank=True)
    period_count = models.IntegerField(default=0, blank=True)
    period_val = models.CharField(max_length=1, choices=PERIODS, blank=True, default='N')

    class Meta:
        ordering = ('deadline',)

    def __str__(self):
        return self.title


class ThreeToFourAdapter:
    @staticmethod
    def __ORM_2_Lib(task: Task):

        lib_task = LibTask(task.title, task.description, task.deadline, task.toad_comment, task.priority,
                           task.created_user.id)
        lib_task.task_id = task.id
        lib_task.user = task.created_user
        lib_task.state = task.get_status_display()
        children = Task.objects.filter(parent=task)
        for sub_task in children:
                lib_task.sub_tasks.append(ThreeToFourAdapter.get_task(id=int(sub_task.id)))
        if task.period_val == 'D':
            lib_task.replan_period = relativedelta(days=task.period_count)
        elif task.period_val == 'W':
            for day in task.repeat_days:
                lib_task.replan_weekdays.append(int(day))
        lib_task.manager_id = task.task_list_id
        #elif task.period_val == 'W':
         #   lib_task.replan_weekdays
        return lib_task

    @staticmethod
    def __convert_sub_tasks(sub_tasks):
        ids = ''
        for sub_task in sub_tasks:
            ids += ' ' + str(sub_task.task_id)
        return ids

    @staticmethod
    def __list_2_Manager(task_list):
        manager = LibManager([task_list.owner])
        usrs = task_list.users.all()
        try:
            iterator = iter(usrs)
            for usr in usrs:
                if usr != task_list.owner:
                    manager.user_list.append(usr)
        except TypeError:
            manager.user_list.append(usrs)
        manager.name = task_list.name
        manager.manager_id = task_list.id
        tasks = ThreeToFourAdapter.filter_tasks(task_list_id=task_list.id)
        for task in tasks:
            sfuncs.add_task_to_dict(manager, task)
        for day in manager.task_dict.keys():
            manager.task_dict[day] = sorted(manager.task_dict[day])

        return manager

    @staticmethod
    def get_task(*args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return LibTask:
        """
        print(*args,)
        print(kwargs)
        task = Task.objects.get(*args, **kwargs)
        print(task.priority)
        print(task.repeat_days)
        lib_task = ThreeToFourAdapter.__ORM_2_Lib(task)
        lib_task.user = User.objects.get(id=task.created_user.id)
        children = Task.objects.filter(parent=task)
        ls = []
        for sub_task in children:
            ls.append(ThreeToFourAdapter.get_task(id=int(sub_task.id)))
        ls = list(set(ls))
        lib_task.sub_tasks = ls
        return lib_task

    @staticmethod
    def save_task(lib_task: LibTask):
        task = Task.objects.get(id=lib_task.task_id)
        task.title = lib_task.name
        task.description = lib_task.description
        task.priority = lib_task.priority
        task.toad_comment = lib_task.toad_comment
        task.deadline = lib_task.last_date
        if isinstance(lib_task.replan_period, relativedelta):
            if lib_task.replan_period.days and lib_task.replan_period.days != 0:
                task.period_count = lib_task.replan_period.days
            elif lib_task.replan_period.months and lib_task.replan_period.months != 0:
                task.period_count = lib_task.replan_period.months
        if lib_task.replan_period == None:
            task.period_count = 0
        task.status = lib_task.state[0]

        #task.sub_task_ids = ThreeToFourAdapter.__convert_sub_tasks(lib_task.sub_tasks)
        try:
            task.parent = Task.objects.get(id=lib_task.parent_id)
        except Task.DoesNotExist:
            pass
        task.created_user =lib_task.user
        print(task.description)
        task.save()

    @staticmethod
    def filter_tasks(*args, **kwargs):
        tasks = Task.objects.filter(*args, **kwargs)
        lib_tasks = []
        for task in tasks:
            lib_tasks.append(ThreeToFourAdapter.__ORM_2_Lib(task))
        return lib_tasks

    @staticmethod
    def get_manager(*args, **kwargs):
        task_list = TaskList.objects.get(*args, **kwargs)
        manager = ThreeToFourAdapter.__list_2_Manager(task_list)

        return manager

    @staticmethod
    def filter_managers(*args, **kwargs):
        lists = TaskList.objects.filter(*args, **kwargs)
        ids = []
        managers= []
        for ls in lists:
            man = ThreeToFourAdapter.__list_2_Manager(ls)
            managers.append(man)


        return managers

    @staticmethod
    def save_manager(manager: LibManager):
        task_list = TaskList.objects.get(id=manager.manager_id)
        task_list.name = manager.name
        task_list.owner = manager.user_list[0]
        task_list.users.clear()
        for usr in manager.user_list[1:]:
            task_list.users.add(usr)
        task_list.save()

