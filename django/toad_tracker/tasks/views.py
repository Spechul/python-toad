from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.db.models import Q
from django.conf import settings
from datetime import datetime
from dateutil.relativedelta import *
from .models import TaskList, Task, ThreeToFourAdapter
from instances.tasktypes import Task as LibTask, State as LibTaskState
from instances.scheduling import Manager as LibManager
from functions import schedulingfunctions as sfuncs
from functions import taskfunctions as tfunc
from .scheduling import check_user_overdue
from django.contrib.auth.models import User
from itertools import chain
# Create your views here.


@login_required
def tasks_main(request):
    context = None
    if request.user.is_authenticated():
        context = {'user': request.user}
    return render(request, 'tasks/main.html', context=context)


@login_required
def add_list(request):
    if request.method == 'POST':
        try:
            name = request.POST['name']
        except KeyError:
            return HttpResponseBadRequest()
        if name == '':
            return render(request, 'tasks/add_list.html')
        user = request.user
        task_list = TaskList(name=name, owner=user)
        task_list.save()
        task_list.users.add(user)
        task_list.save()
        return redirect('/tasks/task-lists')

    return render(request, 'tasks/add_list.html')


@login_required
def add_task(request):
    if request.method == 'POST':
        try:
            title = 'task'
            if request.POST['title'] != '':
                title = request.POST['title']
            description = request.POST['description']
            priority = int(request.POST['priority'])
            list_id = int(request.POST['list_id'])
            deadline = request.POST['deadline']
            period = request.POST['period']
            days = request.POST.getlist('days')
            #print(days)
            count = int(request.POST['count'])
            dd = None
            if deadline != '':
                dd = datetime.strptime(deadline, settings.DATETIME_PATTERN)
        except (KeyError, ValueError, AttributeError):
            context = {'msg': 'please, fill the whole form'}
            return render(request, 'tasks/add_task.html', context=context)

        user = request.user
        task = Task(
            title=title,
            description=description,
            created_user=user,
            priority=priority,
            deadline=dd,
            task_list_id=list_id
        )
        if dd:
            task.period_val = period
            task.period_count = count
            if period == 'W':
                task.repeat_days = days
        task.save()

        return redirect('/tasks/task-lists')
    return render(request, 'tasks/add_task.html')


@login_required
def delete_task(request, task_id, parent_id=None):
    task = get_object_or_404(Task, id=task_id, created_user=request.user)
    try:
        if parent_id:
            parent = get_object_or_404(Task, id=parent_id)

    except (KeyError, AttributeError):
        raise
    task.delete()
    return redirect('/tasks/task-lists')


@login_required
def delete_sub_task(request, task_id, parent_id):
    print(task_id)
    print(parent_id)
    sub_task = get_object_or_404(Task, id=task_id)

    sub_task.delete()
    return redirect('/tasks/task-lists')


@login_required
def complete_task(request, task_id):
    lib_task = ThreeToFourAdapter.get_task(id=task_id)
    print(lib_task.manager_id)
    date = lib_task.last_date
    sfuncs.replan_task_only(lib_task)
    if date == lib_task.last_date:
        tfunc.set_state(lib_task, LibTaskState.COMPLETED)
    ThreeToFourAdapter.save_task(lib_task)
    tsk = Task.objects.get(id=lib_task.task_id)
    print(tsk.status)
    return redirect('/tasks/task-lists/')


@login_required
def task_details(request, task_id):
    tsk = Task.objects.get(id=task_id)
    print(tsk.repeat_days)
    lib_task = ThreeToFourAdapter.get_task(id=task_id)
    if request.method == 'POST':
        try:
            title = request.POST['subtask_name']

        except (AttributeError, KeyError):
            raise
        sub_task = Task(title=title, parent=get_object_or_404(Task, id=task_id), created_user=request.user)
        sub_task.save()
        print(sub_task.id)
        lib_task.sub_tasks.append(ThreeToFourAdapter.get_task(id=sub_task.id))
        ThreeToFourAdapter.save_task(lib_task)

    context = {
        'task': lib_task,
    }
    return render(request, 'tasks/task_details.html', context)


@login_required
def edit_task(request, task_id):
    print(task_id)
    if request.method == 'POST':
        try:
            title = request.POST['title']
            description = request.POST['description']
            priority = int(request.POST['priority'])
            manager_id = request.POST['list_id']
            deadline = request.POST['deadline']
            period = request.POST['period']
            days = request.POST.getlist('days')
            dd = None
            if deadline != '':
                dd = datetime.strptime(deadline, settings.DATETIME_PATTERN)
        except (KeyError, ValueError, AttributeError):
            return HttpResponseBadRequest()
        try:
            task = ThreeToFourAdapter.get_task(id=task_id)
            print(task)
            task.name = title
            task.priority = priority
            task.description = description
            task.manager_id = manager_id

            task.last_date = dd
            print(task.description)
            ThreeToFourAdapter.save_task(task)
            if dd:
                if period == 'W':
                    task.replan_weekdays = days
            if period == 'N':
                task.replan_period = None
                task.replan_weekdays = []
                print('CYKA')
            ThreeToFourAdapter.save_task(task)
        except TaskList.DoesNotExist:
            return HttpResponseBadRequest()
        except Task.DoesNotExist:
            messages.error(request, "You don't have permission to edit this task")

        return redirect('/tasks/task-lists')
    print(task_id)
    task = ThreeToFourAdapter.get_task(id=task_id)
    try:
        deadline = datetime.strftime(task.deadline, settings.DATETIME_PATTERN)
    except (AttributeError, TypeError):
        deadline = None
    context = {
        'task': task,
        'deadline': deadline,
        'selected_days': task.replan_weekdays
    }
    return render(request, 'tasks/edit_task.html', context)


@login_required
def invite_user(request, list_id):
    if request.method == 'POST':
        try:
            user_id = request.POST['user_id']
        except KeyError:
            return HttpResponseBadRequest()
        task_list = get_object_or_404(TaskList, id=list_id, owner=request.user)
        invited_user = get_object_or_404(User, id=int(user_id))

        task_list.users.add(invited_user)
        task_list.save()

        invited_user.save()
        return redirect('/tasks/task-lists/' + str(list_id))

    task_list = get_object_or_404(TaskList, id=list_id)
    context = {
        'users': User.objects.exclude(task_lists__in=[task_list])
    }
    return render(request, 'tasks/invite_user.html', context)


@login_required
def task_lists(request):
    user = request.user
    managers = ThreeToFourAdapter.filter_managers(users__in=[user])
    context = {
        'task_lists': managers,
    }
    return render(request, 'tasks/task_lists.html', context=context)


@login_required
def list_details(request, list_id):
    sort_type = 'deadline'
    if request.method == 'POST':
        try:
            sort_type = request.POST['sort_type']
        except KeyError:
            pass
    task_list = ThreeToFourAdapter.get_manager(id=list_id)
    sfuncs.check_days_overdue(task_list)
    tasks = []
    for ls in task_list.task_dict.values():
        for task in ls:
            tasks.append(task)
            print(task)
    users = task_list.user_list
    context = {
        'tasks': tasks,
        'users': users,
        'list': task_list,
        'owner': request.user == task_list.user_list[0]
    }
    return render(request, 'tasks/list_details.html', context)


@login_required
def delete_list(request, list_id):
    task_list = get_object_or_404(TaskList, id=list_id, owner=request.user)
    task_list.delete()
    return redirect('/tasks/task-lists/')


@login_required
def kick_user(request, list_id, user_id):
    tasklist = get_object_or_404(TaskList, id=list_id, owner=request.user)
    kicked_user = User.objects.get(id=int(user_id))
    tasklist.users.remove(kicked_user)
    tasklist.save()
    return redirect('/tasks/task-lists/' + str(list_id))


@login_required
def tag_details(request):
    context = None
    if request.method == 'POST':
        try:
            search_query = request.POST['search_query']
            if search_query == '':
                raise KeyError

            tasks = ThreeToFourAdapter.filter_tasks(title__startswith=search_query)
            for task in tasks:
                print(task)
            context = {
                'tasks': tasks
            }
        except KeyError:
            return HttpResponseBadRequest()
    return render(request, 'tasks/tag_filter.html', context)


def test(request):
    l_t = ThreeToFourAdapter.get_task(title='123', user=request.user)
    print(l_t, request.user.id)
    return HttpResponse(1)
