from .models import Task
from django.db.models import Q
from django.utils import timezone
from dateutil.relativedelta import *
from dateutil.parser import parser
from datetime import datetime


def check_user_overdue(user):
    tasks = Task.objects.filter(created_user__in=[user])
    for task in tasks:
        date = datetime.now()
        if task.deadline:
            if task.deadline.time() < date.time()\
                    and task.deadline.date() < date.date():
                task.status = 'O'

            task.save()

