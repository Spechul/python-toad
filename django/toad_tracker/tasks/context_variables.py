from .models import TaskList


def make_basic_context(request):
    task_lists = None
    tags = None
    if not request.user.is_anonymous:
        task_lists = TaskList.objects.filter(users__in=[request.user])
    return {
        'task_lists': task_lists,
        'user': request.user,
        'request_path': request.path,
    }