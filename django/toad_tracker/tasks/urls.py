from django.conf.urls import url
from django.http import HttpResponseRedirect
from . import views


urlpatterns = [
    url(r'^add-task/$', views.add_task, name='add_task'),
    url(r'^add-list/$', views.add_list),
    url(r'^main/$', views.tasks_main),

    url(r'^tags/details/(?P<tag_id>\d{0,50})/$', views.tag_details),
    url(r'^tags/search/$', views.tag_details, name='search'),

    url(r'^task-lists/$', views.task_lists),
    url(r'^task-lists/(?P<list_id>\d{0,50})/$', views.list_details),
    url(r'^task-lists/(?P<list_id>\d{0,50})/delete/$', views.delete_list),
    url(r'^task-lists/(?P<list_id>\d{0,50})/invite/$', views.invite_user),
    url(r'^task-lists/(?P<list_id>\d{0,50})/kick/(?P<user_id>\d{0,50})/$', views.kick_user),

    url(r'^task-details/(?P<task_id>\d{1,50})/$', views.task_details),
    url(r'^edit-task/(?P<task_id>\d{0,50})/$', views.edit_task, name="edit_task"),
    url(r'^delete-sub-task/(?P<task_id>\d{0,50})/(?P<parent_id>\d{0,50})$', views.delete_sub_task),
    url(r'^delete-task/(?P<task_id>\d{0,50})/$', views.delete_task),
    url(r'^complete-task/(?P<task_id>\d{0,50})/$', views.complete_task),

    url(r'test', views.test)
]